def calculate_profit(amount: int, percent: float, period: int) -> float:
    start_amount = amount
    for _ in range(period):
        amount *= 1 + percent / 100
    return amount - start_amount
