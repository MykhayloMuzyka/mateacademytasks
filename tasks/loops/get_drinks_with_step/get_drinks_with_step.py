def get_drinks_with_step(number_of_guests: int, step: int) -> int:
    result = 0
    for i in range(1, number_of_guests+1, step):
        result += i
    return result
