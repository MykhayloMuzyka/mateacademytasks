def get_drinks(number_of_guests: int) -> int:
    result = 0
    for i in range(1, number_of_guests+1):
        result += i
    return result