# write your code below this line
def greeter(name: str, part_of_the_day: str) -> str:
    return f"Good {part_of_the_day}, {name}!"