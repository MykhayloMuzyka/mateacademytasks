from math import floor

mean = lambda l: floor(sum(l) / len(l))

def get_speed_statistic(test_results: list) -> list:
    if len(test_results) > 0:
        return [min(test_results), max(test_results), mean(test_results)]
    return [0, 0, 0]