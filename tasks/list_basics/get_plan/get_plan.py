import math as m

add_percent = lambda amount, percent: m.floor(amount + amount * percent / 100)

def get_plan(current_production: int, month: int, percent: int) -> list:
    result = list()
    for _ in range(month):
        if len(result) == 0:
            result.append(add_percent(current_production, percent))
        else:
            result.append(add_percent(result[-1], percent))
    return result