def double_power(current_powers: list) -> list:
    return [power*2 for power in current_powers]