def is_sorted(box_numbers: list) -> bool:
    for i in range(len(box_numbers)-1):
        if box_numbers[i] > box_numbers[i+1]:
            return False
    return True