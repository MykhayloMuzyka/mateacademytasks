def get_location(coordinates: list, commands: list) -> list:
    x, y = coordinates
    for command in commands:
        if command == 'forward':
            y += 1
        elif command == 'back':
            y -= 1
        elif command == 'right':
            x += 1
        else:
            x -= 1
    return [x, y]