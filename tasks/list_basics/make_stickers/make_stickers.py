def make_stickers(details_count: int, robot_part: str) -> list:
    return [f"{robot_part} detail #{i+1}" for i in range(details_count)]
