def get_lists_sum(ls1: list, ls2: list) -> int:
    return sum(ls1) + sum(ls2)