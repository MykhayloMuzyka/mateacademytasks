scroll = lambda string: string[1:] + string[0]


def scrolling_text(string: str) -> list:
    if len(string) > 0:
        result = [string.upper()]
        for _ in range(len(string) - 1):
            result.append(scroll(result[-1]))
        return result
    return list()