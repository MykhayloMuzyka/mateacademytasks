is_positive = lambda num: num > 0
is_multiple = lambda num1, num2: num1 % num2 == 0


def check_number(number: int) -> list:
    return [is_positive(number), is_multiple(number, 2), is_multiple(number, 10)]