description_simple_math_checker = """
Створіть функцію check_number, яка перевіряє число на три різні властивості:

Це позитивне число?
Це парне число?
Це число кратне 10?
Функція check_number повинна повернути список з результатами перевірок у вигляді булевих значень.

Число завжди має бути цілим.

Приклад:

check_number(3) == [True, False, False]
check_number(10) == [True, True, True]
check_number(0) == [False, True, True]
check_number(-1) == [False, False, False]
"""