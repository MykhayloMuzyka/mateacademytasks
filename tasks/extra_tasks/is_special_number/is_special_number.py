def divide_number_by_digits(number: int) -> list:
    result = list()
    str_number = str(number)
    for i in range(len(str(number))):
        result.append(int(str_number[-1]))
        str_number = str_number[:len(str_number)-1]
    return result[::-1]


def is_special_number(number: int) -> str:
    digits = divide_number_by_digits(number)
    for num in digits:
        if not 0 <= num <= 5:
            return 'NOT!!'
    return 'Special!!'