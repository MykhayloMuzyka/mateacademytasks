def combine_lists(ls1: list, ls2: list) -> list:
    return list(map(lambda a, b: a + b, ls1, ls2))