description_combine_lists = """
Реалізуй функцію combine_arrays, яка приймає 2 списки чисел (ls1 та ls2) та повертає список чисел де result_list[i] це сума чисел ls1[i] та ls2[i].

Примітки:

Вхідні списки завжди однакового розміру.
Приклади:

combine_arrays([1, 2, 5], [3, 6, 1]) == [4, 8, 6]
combine_arrays([1], [6]) == [7]
combine_arrays([], []) == []
"""