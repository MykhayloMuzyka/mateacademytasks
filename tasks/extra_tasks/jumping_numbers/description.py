description_jumping_numbers = """
Реалізуй функцію is_jumping, яка приймає число number та повертає рядок JUMPING, якщо кожна цифра в числі відрізняється від сусідньої на 1. Якщо умова не виконується - рядок NOT JUMPING.

Примітки:

Вхідне число завжди додатнє
Різниця між 9 та 0 не розцінюється як 1
Всі числа, які складаються із однієї цифри - JUMPING
Приклади:

is_jumping(9) == 'JUMPING'
It's single-digit number
is_jumping(79) == 'NOT JUMPING'
Adjacent digits don't differ by 1
is_jumping(23454) == 'JUMPING'
Adjacent digits differ by 1
"""