def divide_number_by_digits(number: int) -> list:
    result = list()
    str_number = str(number)
    for i in range(len(str(number))):
        result.append(int(str_number[-1]))
        str_number = str_number[:len(str_number)-1]
    return result[::-1]


def is_jumping(number: int) -> str:
    digits = divide_number_by_digits(number)
    for i in range(len(digits) - 1):
        if digits[i] - 1 != digits[i+1] and digits[i] + 1 != digits[i+1]:
            return 'NOT JUMPING'
    return 'JUMPING'