def split_string(string: str) -> list:
    result = ['']
    for symbol in string:
        if len(result[-1]) == 2:
            result.append('')
        result[-1] += symbol
    if len(result[-1]) == 1:
        result[-1] += '_'
    return result