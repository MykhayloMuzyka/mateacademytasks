def get_tips_rating(amount: int) -> str:
    if amount == 0:
        return 'terrible'
    elif amount <= 10:
        return 'poor'
    elif amount <= 20:
        return 'good'
    elif amount <= 50:
        return 'great'
    else:
        return 'excellent'