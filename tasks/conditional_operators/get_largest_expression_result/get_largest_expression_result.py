def get_largest_expression_result(a: float, b: float) -> float:
    return max([a + b, a - b, a * b, a / b])