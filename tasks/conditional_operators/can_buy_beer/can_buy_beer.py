def can_buy_beer(age: int) -> str:
    if age >= 18:
        return 'You can buy beer'
    else:
        return 'You can not buy beer'