def calculate_taxes(income: int) -> float:
    if income <= 1000:
        return income * 0.02
    elif income <= 10000:
        return income * 0.03
    else:
        return income * 0.05