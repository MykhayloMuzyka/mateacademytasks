def remove_vowels(document: str) -> str:
    vowels = 'aeiouy'
    result = ''
    for symbol in document:
        if symbol.lower() not in vowels:
            result += symbol
    return result