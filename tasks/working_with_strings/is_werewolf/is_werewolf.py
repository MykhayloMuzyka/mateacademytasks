def is_werewolf(target: str) -> bool:
    clear_target = [symbol for symbol in target if symbol.isalpha()]
    for i in range(len(clear_target)//2):
        if clear_target[i].lower() != clear_target[-(i+1)].lower():
            return False
    return True