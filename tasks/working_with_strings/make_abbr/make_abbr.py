def make_abbr(words: str) -> str:
    if len(words) > 0:
        list_of_words = words.split(' ')
        result = ''
        for word in list_of_words:
            result += word[0]
        return result.upper()
    return ''