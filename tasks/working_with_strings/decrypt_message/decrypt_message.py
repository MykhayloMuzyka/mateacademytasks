def decrypt_message(message: str) -> str:
    return message[::-1]
