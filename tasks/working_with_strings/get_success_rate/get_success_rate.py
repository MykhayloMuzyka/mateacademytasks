def get_success_rate(statistics: str) -> int:
    understand_student = 0
    for student in statistics:
        if student == '1':
            understand_student += 1
    return round(understand_student / len(statistics) * 100)